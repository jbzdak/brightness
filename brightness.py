#!/usr/bin/env python3
"""

Program to

USAGE:
    brightness up
    brightness down
    brightness info

up  Increase brightness
down  Decrease brightness
info  Print information about current display
"""

import sys
import os
import docopt

d = docopt.docopt(__doc__)


class NVBrightnessManager(object):
    """
    NVidia brightness using open source drivers.

    Thinkpad W530 (dedicated card)
    """

    NAME = 'nvidia'

    BACKLIGHT = '/sys/class/backlight/nv_backlight/brightness'
    LEVELS = []
    LEVELS.extend(range(0, 20, 1))
    LEVELS.extend(range(20, 50, 5))
    LEVELS.extend(range(50, 101, 10))


class ProprietaryNVManager(NVBrightnessManager):
    """
    NVidia brightness using proprietary drivers.

    Thinkpad W530 (dedicated card)
    """
    NAME = 'proprietary nvidia'

    BACKLIGHT = '/sys/class/backlight/acpi_video0/brightness'


class IntelBrightnessManager(object):
    """
    Intel brightness.

    Thinkpad W530 (no dedicated card)
    """

    NAME = 'intel'

    BACKLIGHT = '/sys/class/backlight/intel_backlight/brightness'

    LEVELS = []
    LEVELS.extend(range(10, 101, 10))
    LEVELS.extend(range(200, 1000, 100))
    LEVELS.extend(range(1001, 4002, 500))


class AMDVegaManager(object):

    NAME = 'vega'

    BACKLIGHT = '/sys/class/backlight/amdgpu_bl0/brightness'

    LEVELS = []
    LEVELS.extend(range(10, 255, 10))
    LEVELS.append(255)

BACKLIGHTS = [IntelBrightnessManager, NVBrightnessManager, ProprietaryNVManager, AMDVegaManager]


def select():
    for b in BACKLIGHTS:
        if os.path.exists(b.BACKLIGHT):
            b.LEVELS = sorted(set(b.LEVELS))
            b.MIN = min(b.LEVELS)
            b.MAX = max(b.LEVELS)
            return b
    raise ValueError("Unknown backlight")


b = select()


if d['info']:
    print("Currently running on '{}'".format(b.NAME))
    print("Brightness ranges from {} to {} (levels: {})".format(
        min(b.LEVELS), max(b.LEVELS), b.LEVELS)
    )
    print("Backlight file {}".format(b.BACKLIGHT))
    sys.exit(0)
else:
    with open(b.BACKLIGHT) as f:
        BRIGHTNESS = float(f.read())

    current = b.LEVELS.index(min(b.LEVELS, key=lambda x: abs(x - BRIGHTNESS)))

    if d['up']:
        sign = +1
    else:
        sign = -1

    result_idx = max(0, min(len(b.LEVELS) - 1, current + sign))

    RESULT = b.LEVELS[result_idx]
    # print(current, result_idx, RESULT)

if RESULT > b.MAX:
    RESULT = b.MAX

if RESULT < b.MIN:
    RESULT = b.MIN

with open(b.BACKLIGHT, 'w') as f:
    # print(str(RESULT))
    f.write(str(RESULT))
    f.write('\n')
