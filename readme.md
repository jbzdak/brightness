# Brightness manager

CLI command to control brightness levels in my laptops.

Why I use it
------------

i3-wm (display manager I use) has very sparse tooling, and brightness buttons
don't work out of the box. So I created a small wrapper that can be attached 
like that: 

    bindcode $mod+75 exec --no-startup-id sudo brightness.py up 

This is not a generic tool --- won't work out of the box, as ``/sys/class/brightness`` 
API is not really uniform (maximal brightness can be different, some backends
silently accept only discrete values e.g. 50 and 60 and will ignore 55). 

Similar tools
-------------

* I used it instead of [xbacklight](https://github.com/tcatm/xbacklight) 
  because I didn't know about it at the time (and now it doesn't seem to work 
  on my new laptop).
* [acpilight](https://gitlab.com/wavexx/acpilight) also uses ``/sys/class/brightness``
  API but is generic. Probably you should use that.        
 
If you use this tool, and adapt it to your laptop feel free to send a pull/merge request.

Usage
------------

Install ``docopt`` package in global python interpreter (e.g. ``apt get install python-docopt``)

* sudo brightness.py up 
* sudo brightness.py down  